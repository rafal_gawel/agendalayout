//
//  AgendaLayout.swift
//  AgendaLayout
//
//  Created by Rafał Gaweł on 07/09/2018.
//  Copyright © 2018 Rafał Gaweł. All rights reserved.
//

import Foundation

public struct ActivityRange {
    let start: Int
    let length: Int
}

public struct ActivityFrame: Equatable {
    let x: Int
    let y: Int
    let width: Int
    let height: Int
}

struct IndexedActivityRange {
    let index: Int
    let start: Int
    let length: Int
    
    init(index: Int, range: ActivityRange) {
        self.index = index
        self.start = range.start
        self.length = range.length
    }
    
    var end: Int { return start + length }
    
    func overlaps(other: IndexedActivityRange) -> Bool {
        return start < other.end && other.start < end
    }
}

public class IndexedActivityFrame {
    let index: Int
    var x: Int
    let y: Int
    var width: Int
    let height: Int
    
    var isLocked = false
    
    init(index: Int, x: Int, y: Int, width: Int, height: Int) {
        self.index = index
        self.x = x
        self.y = y
        self.width = width
        self.height = height
    }
    
    func rightEdgeTouchesLeftEdge(other: IndexedActivityFrame) -> Bool {
        return y < other.y + other.height && other.y < y + height && x + width == other.x
    }
    
    func rightCollidesWithLeft(other: IndexedActivityFrame) -> Bool {
        return y < other.y + other.height && other.y < y + height && x + width == other.x + 1
    }
}

public func layoutAgenda(activityRanges: [ActivityRange], viewWidth: Int) -> [ActivityFrame] {
    if activityRanges.isEmpty {
        return []
    }
    
    let indexedActivityRanges = activityRanges.enumerated().map(IndexedActivityRange.init)
    
    let groups = makeGroups(activityRanges: indexedActivityRanges)
    
    return groups
        .flatMap { layoutGroup(activityRanges: $0, viewWidth: viewWidth) }
        .sorted { $0.index < $1.index }
        .map { ActivityFrame(x: $0.x, y: $0.y, width: $0.width, height: $0.height) }
}

private func makeGroups(activityRanges: [IndexedActivityRange]) -> [[IndexedActivityRange]] {
    let sortedRanges = activityRanges.sorted { $0.start < $1.start }
    
    var groups: [[IndexedActivityRange]] = []
    var currentGroup: [IndexedActivityRange] = []
    
    for activityRange in sortedRanges {
        if currentGroup.isEmpty || currentGroup.contains(where: { $0.overlaps(other: activityRange) }) {
            currentGroup.append(activityRange)
        } else {
            groups.append(currentGroup)
            currentGroup = [activityRange]
        }
    }
    
    groups.append(currentGroup)
    
    return groups
}

private func layoutGroup(activityRanges: [IndexedActivityRange], viewWidth: Int) -> [IndexedActivityFrame] {
    let columns = makeColumns(activityRanges: activityRanges)
    
    let columnWidth = viewWidth / columns.count
    
    let activityFrames = columns.enumerated().flatMap { (arg) -> [IndexedActivityFrame] in
        let (columnIndex, column) = arg
        return column.map { range in
            IndexedActivityFrame(index: range.index, x: columnIndex * columnWidth, y: range.start, width: columnWidth, height: range.length)
        }
    }
    
    expandFrames(activityFrames: activityFrames, viewWidth: viewWidth)
    
    return activityFrames
}

private func expandFrames(activityFrames: [IndexedActivityFrame], viewWidth: Int) {
    for activityFrame in activityFrames {
        if activityFrame.x + activityFrame.width == viewWidth {
            activityFrame.isLocked = true
        }
    }
    
    for activityFrame in activityFrames.reversed() where !activityFrame.isLocked {
        for possibleColliding in activityFrames where possibleColliding.isLocked {
            if activityFrame.rightEdgeTouchesLeftEdge(other: possibleColliding) {
                activityFrame.isLocked = true
            }
        }
    }
    
    var unlockedFrames = activityFrames.reversed().filter { !$0.isLocked }
    
    while !unlockedFrames.isEmpty {
        for unlockedFrame in unlockedFrames where !unlockedFrame.isLocked {
            unlockedFrame.width += 1
            if unlockedFrame.x + unlockedFrame.width == viewWidth {
                unlockedFrame.isLocked = true
            } else {
                pushCollidedFrames(frame: unlockedFrame, unlockedFrames: unlockedFrames, frames: activityFrames, viewWidth: viewWidth)
                
                if !unlockedFrame.isLocked {
                    if activityFrames.contains(where: { $0.isLocked && unlockedFrame.rightEdgeTouchesLeftEdge(other: $0) }) {
                        unlockedFrame.isLocked = true
                    }
                }
            }
            
            if unlockedFrame.isLocked {
                lockFramesToTheLeft(frame: unlockedFrame, unlockedFrames: unlockedFrames)
            }
        }

        unlockedFrames = unlockedFrames.filter { !$0.isLocked }
    }
}

private func pushCollidedFrames(frame: IndexedActivityFrame, unlockedFrames: [IndexedActivityFrame], frames: [IndexedActivityFrame], viewWidth: Int) {
    for possibleColliding in unlockedFrames where frame.index != possibleColliding.index {
        if frame.rightCollidesWithLeft(other: possibleColliding) {
            possibleColliding.x += 1
            if possibleColliding.x + possibleColliding.width == viewWidth {
                possibleColliding.isLocked = true
                lockFramesToTheLeft(frame: possibleColliding, unlockedFrames: unlockedFrames)
            } else {
                if frames.contains(where: { $0.isLocked && possibleColliding.rightEdgeTouchesLeftEdge(other: $0) }) {
                    possibleColliding.isLocked = true
                }
                
                pushCollidedFrames(frame: possibleColliding, unlockedFrames: unlockedFrames, frames: frames, viewWidth: viewWidth)
            }
            
            if possibleColliding.isLocked {
                frame.isLocked = true
                lockFramesToTheLeft(frame: frame, unlockedFrames: unlockedFrames)
            }
        }
    }
}

func lockFramesToTheLeft(frame: IndexedActivityFrame, unlockedFrames: [IndexedActivityFrame]) {
    for unlockedFrame in unlockedFrames where !unlockedFrame.isLocked && frame.index != unlockedFrame.index {
        if unlockedFrame.rightEdgeTouchesLeftEdge(other: frame) {
            unlockedFrame.isLocked = true
            lockFramesToTheLeft(frame: unlockedFrame, unlockedFrames: unlockedFrames)
        }
    }
}

private func makeColumns(activityRanges: [IndexedActivityRange]) -> [[IndexedActivityRange]] {
    let sortedRanges = activityRanges.sorted { first, second in
        if first.end == second.end {
            return first.start > second.start
        }
        return first.end < second.end
    }
    
    var columns: [[IndexedActivityRange]] = []
    
    for activityRange in sortedRanges {
        var columnFound = false
        
        for (index, column) in columns.enumerated() {
            if !column.contains(where: { $0.overlaps(other: activityRange) }) {
                columns[index].append(activityRange)
                columnFound = true
                break
            }
        }
        
        if !columnFound {
            columns.append([activityRange])
        }
    }
    
    return columns
}
