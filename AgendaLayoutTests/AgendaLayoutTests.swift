//
//  AgendaLayoutTests.swift
//  AgendaLayoutTests
//
//  Created by Rafał Gaweł on 07/09/2018.
//  Copyright © 2018 Rafał Gaweł. All rights reserved.
//

import XCTest
import Foundation
@testable import AgendaLayout

class AgendaLayoutTests: XCTestCase {
    
    func testNoActivities() {
        let activityRanges: [ActivityRange] = []
        let activityFrames = layoutAgenda(activityRanges: activityRanges, viewWidth: 100)
        
        XCTAssertEqual(activityFrames, [])
    }
    
    func testOneActivity() {
        let activityRanges = [ActivityRange(start: 0, length: 10)]
        let activityFrames = layoutAgenda(activityRanges: activityRanges, viewWidth: 100)
        
        XCTAssertEqual(activityFrames, [ActivityFrame(x: 0, y: 0, width: 100, height: 10)])
    }
    
    func testTwoNonIntersectingActivities() {
        let activityRanges = [
            ActivityRange(start: 0, length: 5),
            ActivityRange(start: 5, length: 5)
        ]
        let activityFrames = layoutAgenda(activityRanges: activityRanges, viewWidth: 100)
        
        XCTAssertEqual(activityFrames, [
            ActivityFrame(x: 0, y: 0, width: 100, height: 5),
            ActivityFrame(x: 0, y: 5, width: 100, height: 5)
            ])
    }
    
    func testTwoIntersectingActivities() {
        let activityRanges = [
            ActivityRange(start: 0, length: 5),
            ActivityRange(start: 4, length: 5)
        ]
        let activityFrames = layoutAgenda(activityRanges: activityRanges, viewWidth: 100)
        
        XCTAssertEqual(activityFrames, [
            ActivityFrame(x: 0, y: 0, width: 50, height: 5),
            ActivityFrame(x: 50, y: 4, width: 50, height: 5)
            ])
    }
    
    func testThreeIntersectingActivitiesInTwoColumns() {
        let activityRanges = [
            ActivityRange(start: 0, length: 5),
            ActivityRange(start: 3, length: 5),
            ActivityRange(start: 5, length: 5)
        ]
        let activityFrames = layoutAgenda(activityRanges: activityRanges, viewWidth: 100)
        
        XCTAssertEqual(activityFrames, [
            ActivityFrame(x: 0, y: 0, width: 50, height: 5),
            ActivityFrame(x: 50, y: 3, width: 50, height: 5),
            ActivityFrame(x: 0, y: 5, width: 50, height: 5)
            ])
    }
    
    func testDifferentColumnSizes() {
        let activityRanges = [
            ActivityRange(start: 0, length: 10),
            ActivityRange(start: 0, length: 5),
            ActivityRange(start: 0, length: 5),
            ActivityRange(start: 5, length: 5),
            ActivityRange(start: 5, length: 5),
            ActivityRange(start: 5, length: 5)
        ]
        let activityFrames = layoutAgenda(activityRanges: activityRanges, viewWidth: 100)
        
        XCTAssertEqual(activityFrames, [
            ActivityFrame(x: 75, y: 0, width: 25, height: 10),
            ActivityFrame(x: 0, y: 0, width: 37, height: 5),
            ActivityFrame(x: 37, y: 0, width: 38, height: 5),
            ActivityFrame(x: 0, y: 5, width: 25, height: 5),
            ActivityFrame(x: 25, y: 5, width: 25, height: 5),
            ActivityFrame(x: 50, y: 5, width: 25, height: 5),
            ])
    }
    
    func testFinal() {
        let activityRanges = [
            ActivityRange(start: 0, length: 10),
            ActivityRange(start: 0, length: 5),
            ActivityRange(start: 0, length: 5),
            ActivityRange(start: 0, length: 5),
            ActivityRange(start: 0, length: 5),
            ActivityRange(start: 5, length: 5),
            ActivityRange(start: 5, length: 2),
            ActivityRange(start: 7, length: 3),
            ActivityRange(start: 7, length: 3)
        ]
        let activityFrames = layoutAgenda(activityRanges: activityRanges, viewWidth: 75)
        
        XCTAssertEqual(activityFrames, [
            ActivityFrame(x: 60, y: 0, width: 15, height: 10),
            ActivityFrame(x: 0, y: 0, width: 15, height: 5),
            ActivityFrame(x: 15, y: 0, width: 15, height: 5),
            ActivityFrame(x: 30, y: 0, width: 15, height: 5),
            ActivityFrame(x: 45, y: 0, width: 15, height: 5),
            ActivityFrame(x: 40, y: 5, width: 20, height: 5),
            ActivityFrame(x: 0, y: 5, width: 40, height: 2),
            ActivityFrame(x: 0, y: 7, width: 20, height: 3),
            ActivityFrame(x: 20, y: 7, width: 20, height: 3)
            ])
    }
    
    func testNoSpace() {
        let activityRanges = [
            ActivityRange(start: 0, length: 10),
            ActivityRange(start: 0, length: 10),
        ]
        let activityFrames = layoutAgenda(activityRanges: activityRanges, viewWidth: 1)
        
        XCTAssertEqual(activityFrames, [
            ActivityFrame(x: 0, y: 0, width: 0, height: 10),
            ActivityFrame(x: 0, y: 0, width: 1, height: 10)
        ])
    }
    
    func testSeparator() {
        let activityRanges = [
            ActivityRange(start: 0, length: 5),
            ActivityRange(start: 1, length: 9),
            ActivityRange(start: 2, length: 3),
            ActivityRange(start: 3, length: 2),
            ActivityRange(start: 5, length: 5),
            ActivityRange(start: 5, length: 5)
        ]
        
        let activityFrames = layoutAgenda(activityRanges: activityRanges, viewWidth: 100)
        
        XCTAssertEqual(activityFrames, [
            ActivityFrame(x: 50, y: 0, width: 25, height: 5),
            ActivityFrame(x: 75, y: 1, width: 25, height: 9),
            ActivityFrame(x: 25, y: 2, width: 25, height: 3),
            ActivityFrame(x: 0, y: 3, width: 25, height: 2),
            ActivityFrame(x: 0, y: 5, width: 37, height: 5),
            ActivityFrame(x: 37, y: 5, width: 38, height: 5)
            ])
    }
    
    func testPerformance() {
        let activities = (0..<100).map { _ in
            return ActivityRange(start: Int(arc4random_uniform(500)), length: Int(arc4random_uniform(500)) + 1)
        }
        
        self.measure {
            _ = layoutAgenda(activityRanges: activities, viewWidth: 1000)
        }
    }
    
}
